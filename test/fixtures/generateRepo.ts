import Chance from 'chance';
const chance = new Chance();

export const generateRepo = () => ({
  id: chance.integer({ min: 1000000, max: 9999999 }),
  name: chance.word(),
  full_name: chance.word(),
  description: chance.sentence(),
  url: chance.url(),
  homepage: chance.url(),
  stargazers_count: chance.integer({ min: 0, max: 50000 }),
  language: chance.pickone(['js', 'ts', 'elixir', 'ruby', 'rust', 'c']),
  owner: {
    avatar_url: 'https://avatar-url.none/get',
    login: chance.name(),
  },
});

export const generateReposArray = (num: number) =>
  [...new Array(num)].map(generateRepo);
