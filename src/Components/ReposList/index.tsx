import React from 'react';

import { Repo } from '../../App';

import './ReposList.css';

export const ReposList = ({
  repos,
  selected,
  setSelected,
  bookmarksIds,
  setBookmark,
}: {
  repos: Repo[];
  selected: null | number;
  setSelected: (arg0: number) => void;
  bookmarksIds: number[];
  setBookmark: (arg0: number) => void;
}) => (
  <section data-testid="repos-list-section" className="repos-list flex flex-col mb-14">
    {repos.map((repo) => (
      <div
        key={repo.id}
        data-testid="repo-item"
        className="repo-item flex flex-col border-b border-gray-200 border-solid hover:shadow-md rounded my-2 p-4"
      >
        <div className="flex flex-row justify-between">
          <div
            onClick={() => setSelected(repo.id)}
            className="font-semibold text-lg pb-4 cursor-pointer"
          >
            {repo.name} - {repo.stargazers_count}
          </div>
          <div onClick={() => setBookmark(repo.id)} data-testid="repo-bookmark-link" className="cursor-pointer">
            {bookmarksIds.some((i) => i === repo.id)
              ? 'Remove bookmark'
              : 'Add bookmark'}
          </div>
        </div>
        {selected === repo.id && (
          <div className="repo-details flex flex-row">
            <div className="pr-4">
              <img
                alt={`${repo.owner.login} avatar`}
                src={repo.owner.avatar_url}
                className="h-16"
              />
              {repo.owner.login}
            </div>
            <div>
              <div>{repo.description}</div>
              <div>
                <a
                  href={repo.url}
                  target="_blank"
                  rel="noreferrer"
                  className="text-blue-400 hover:text-blue-500"
                >
                  {repo.url}
                </a>
              </div>
            </div>
          </div>
        )}
      </div>
    ))}
  </section>
);
