import React from 'react';
import './Loading.css';

export const Loading = () => (
  <div className="loading">
    <div data-testid="loading">Loading...</div>
  </div>
);
