import React from 'react';
import { Links } from 'parse-link-header';
import ReactPaginate from 'react-paginate';

export const Pagination = ({
  pagination,
  setPage,
}: {
  pagination: Links;
  setPage: (arg0: number) => void;
}) => (
  <nav data-testid="pagination-wrapper" className="fixed overflow-hidden bottom-0 w-full bg-white">
    <ReactPaginate
      pageCount={Number(pagination.last.page)}
      pageRangeDisplayed={5}
      marginPagesDisplayed={3}
      onPageChange={({ selected }) => setPage(selected)}
      containerClassName="p-4"
      disabledClassName="text-gray-300 p-1 hover:shadow-none"
      nextClassName="inline-block p-1 hover:shadow-md rounded"
      previousClassName="inline-block p-1 hover:shadow-md rounded"
      pageClassName="inline-block p-1 hover:shadow-md rounded"
      breakClassName="inline-block p-1 hover:shadow-md rounded"
      activeClassName="active text-yellow-400 font-bold border border-gray-200 border-solid hover:shadow-md rounded"
    />
  </nav>
);
