import React from 'react';

export const Navigation = ({
  bookmarksLength,
  total,
  reposListUi,
  bookmarksUi,
}: {
  bookmarksLength: number;
  total: number;
  reposListUi: () => void;
  bookmarksUi: () => void;
}) => (
  <nav>
    <ul className="cursor-pointer text-xl text-blue-400">
      <li
        onClick={reposListUi}
        className="inline-block mr-8 hover:text-blue-500"
      >
        Total: {total}
      </li>
      <li onClick={bookmarksUi} className="inline-block hover:text-blue-500">
        Bookmarks: {bookmarksLength}
      </li>
    </ul>
  </nav>
);
