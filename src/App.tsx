import React, { useState, useReducer, useEffect } from 'react';
import parseLink, { Links } from 'parse-link-header';

import { Navigation } from './Components/Navigation';
import { ReposList } from './Components/ReposList';
import { Pagination } from './Components/Pagination';
import { Loading } from './Components/Loading';

import './App.css';

enum NAVIGATION {
  REPOS_LIST = 'reposList',
  BOOKMARKS = 'bookmarks',
}

enum ACTION_TYPES {
  SUCCESS = 'fetchReposSuccess',
  FAILURE = 'fetchReposFailure',
  SET_PAGE = 'setPage',
  SET_SELECTED = 'setSelected',
  SET_BOOKMARK = 'setBookMark',
  REHYDRATE_BOOKMARKS = 'rehydrateBookmarks',
}

export type Repo = {
  id: number;
  name: string;
  full_name: string;
  description: string;
  url: string;
  homepage: string;
  stargazers_count: number;
  language: string;
  owner: {
    avatar_url: string;
    login: string;
  };
};

type GithubRepoResponse = {
  total_count: number;
  incomplete_results: boolean;
  items: Repo[];
};

type Action =
  | {
      type: ACTION_TYPES.SUCCESS;
      payload: { pagination: null | Links; data: GithubRepoResponse };
    }
  | { type: ACTION_TYPES.FAILURE }
  | { type: ACTION_TYPES.SET_PAGE; payload: number }
  | { type: ACTION_TYPES.SET_SELECTED; payload: number }
  | { type: ACTION_TYPES.SET_BOOKMARK; payload: number }
  | { type: ACTION_TYPES.REHYDRATE_BOOKMARKS; payload: Repo[] };

type State = {
  pagination: null | Links;
  total: number;
  repos: Repo[];
  bookmarks: Repo[];
  selected: null | number;
  page: number;
  errApi: boolean;
  isLoading: boolean;
};

const initialState: State = {
  pagination: null,
  total: 0,
  repos: [],
  bookmarks: [],
  selected: null,
  page: 1,
  errApi: false,
  isLoading: true,
};

const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case ACTION_TYPES.SUCCESS:
      return {
        ...state,
        isLoading: false,
        total: action.payload.data.total_count,
        repos: action.payload.data.items,
        pagination: action.payload.pagination,
        selected: null,
      };
    case ACTION_TYPES.FAILURE:
      return {
        ...state,
        isLoading: false,
        errApi: true,
        pagination: null,
        selected: null,
        page: 1,
        repos: [],
        total: 0,
      };
    case ACTION_TYPES.SET_PAGE:
      return { ...state, page: action.payload, isLoading: true, errApi: false };
    case ACTION_TYPES.SET_SELECTED:
      return {
        ...state,
        selected: state.selected === action.payload ? null : action.payload,
      };
    case ACTION_TYPES.SET_BOOKMARK:
      const filteredBookmarks = state.bookmarks.filter(
        (b) => b.id !== action.payload,
      );

      if (filteredBookmarks.length !== state.bookmarks.length) {
        return { ...state, bookmarks: filteredBookmarks };
      }

      const repo = state.repos.find((r) => r.id === action.payload);

      if (!repo) return state;
      return { ...state, bookmarks: [...filteredBookmarks, repo] };
    case ACTION_TYPES.REHYDRATE_BOOKMARKS:
      return { ...state, bookmarks: action.payload };
    default:
      throw new Error();
  }
};

function App() {
  const [
    { pagination, total, repos, bookmarks, selected, page, errApi, isLoading },
    dispatch,
  ] = useReducer(reducer, initialState);
  // router :)
  const [uiSection, setUiSection] = useState(NAVIGATION.REPOS_LIST);

  useEffect(() => {
    const loadRepos = async () => {
      let weekAgo = new Date();
      weekAgo.setDate(weekAgo.getDate() - 7);

      try {
        const res = await fetch(
          //language:....
          `https://api.github.com/search/repositories?q=created:>${
            weekAgo.toISOString().split('T')[0]
          }&sort=stars&order=desc&page=${page}`,
          {
            headers: {
              'Content-Type': 'application/vnd.github.v3+json',
            },
          },
        );
        const json = await res.json();

        dispatch({
          type: ACTION_TYPES.SUCCESS,
          payload: {
            pagination: parseLink(res.headers.get('Link') || ''),
            data: json,
          },
        });
      } catch {
        dispatch({ type: ACTION_TYPES.FAILURE });
      }
    };
    loadRepos();
  }, [page]);

  useEffect(() => {
    const storedBookmarks = localStorage.getItem('bookmarks') || '[]';
    dispatch({
      type: ACTION_TYPES.REHYDRATE_BOOKMARKS,
      payload: JSON.parse(storedBookmarks),
    });
  }, []);

  useEffect(() => {
    localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
  }, [bookmarks]);

  return (
    <div className="App container mx-auto px-4">
      <header>
        <Navigation
          bookmarksLength={bookmarks.length}
          total={total}
          reposListUi={() => setUiSection(NAVIGATION.REPOS_LIST)}
          bookmarksUi={() => setUiSection(NAVIGATION.BOOKMARKS)}
        />
      </header>
      {/* TODO: block actions when isLoading */}
      <ReposList
        repos={uiSection === NAVIGATION.REPOS_LIST ? repos : bookmarks}
        selected={selected}
        setSelected={(id) =>
          dispatch({ type: ACTION_TYPES.SET_SELECTED, payload: id })
        }
        bookmarksIds={bookmarks.map((b) => b.id)}
        setBookmark={(id) =>
          dispatch({ type: ACTION_TYPES.SET_BOOKMARK, payload: id })
        }
      />
      <footer>
        {pagination !== null && uiSection === NAVIGATION.REPOS_LIST && (
          <Pagination
            pagination={pagination}
            setPage={(page) =>
              dispatch({ type: ACTION_TYPES.SET_PAGE, payload: page })
            }
          />
        )}
      </footer>
      {isLoading && <Loading />}
    </div>
  );
}

export default App;
