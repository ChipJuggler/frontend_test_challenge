import React from 'react';
import {
  render,
  screen,
  waitFor,
  waitForElementToBeRemoved,
  within,
  fireEvent,
} from '@testing-library/react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import * as path from 'path';
import * as fs from 'fs';

import { generateReposArray } from '../test/fixtures/generateRepo';
import App from './App';

const repos = generateReposArray(10);

const server = setupServer(
  rest.get('https://api.github.com/search/repositories', (req, res, ctx) => {
    return res(
      ctx.set(
        'link',
        '<https://api.github.com/search/repositories?q=created%3A%3E2021-02-14&sort=stars&order=desc&page=2>; rel="next", <https://api.github.com/search/repositories?q=created%3A%3E2021-02-14&sort=stars&order=desc&page=34>; rel="last"',
      ),
      ctx.json({
        total_count: repos.length,
        incomplete_results: false,
        items: repos,
      }),
    );
  }),
  rest.get('https://avatar-url.none/get', (req, res, ctx) => {
    const imageBuffer = fs.readFileSync(
      path.resolve(__dirname, '../test/fixtures/avatar.jpg'),
    );

    return res(
      ctx.set('Content-Length', imageBuffer.byteLength.toString()),
      ctx.set('Content-Type', 'image/jpeg'),
      ctx.body(imageBuffer),
    );
  }),
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('App flows', () => {
  test('renders repos list with pagination', async () => {
    render(<App />);
    const loading = screen.getByTestId('loading');

    await waitForElementToBeRemoved(loading);
    await waitFor(() => screen.getByText(/Total: 10/i));

    const paginationWrapper = screen.getByTestId('pagination-wrapper');
    within(paginationWrapper).getByText('34');

    const reposListSection = screen.getByTestId('repos-list-section');
    const repos = within(reposListSection).getAllByTestId('repo-item');
    expect(repos.length).toEqual(10);
  });

  // TODO: simplify it by extracting additional flows into ReposList tests
  test('bookmarks functionality', async () => {
    render(<App />);

    await waitFor(() => screen.getByText(/Total: 10/i));
    screen.getByText(/Bookmarks: 0/i);

    fireEvent.click(screen.getAllByTestId('repo-bookmark-link')[3]);
    await waitFor(() => screen.getByText(/Bookmarks: 1/i));

    fireEvent.click(screen.getAllByTestId('repo-bookmark-link')[3]);
    await waitFor(() => screen.getByText(/Bookmarks: 0/i));

    fireEvent.click(screen.getAllByTestId('repo-bookmark-link')[2]);
    await waitFor(() => screen.getByText(/Bookmarks: 1/i));

    fireEvent.click(screen.getByText(/Bookmarks: 1/i));

    expect(screen.queryByTestId('pagination-wrapper')).toBeNull();

    expect(screen.getAllByTestId('repo-item').length).toEqual(1);
    screen.getByText(`${repos[2].name} - ${repos[2].stargazers_count}`);

    fireEvent.click(screen.getAllByTestId('repo-bookmark-link')[0]);
    expect(screen.queryAllByTestId('repo-item').length).toEqual(0);
  });
});
